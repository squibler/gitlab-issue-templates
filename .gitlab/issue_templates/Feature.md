### USER STORY
**As a**
[ DESCRIBE THE ROLE e.g. MANGER or WEBSITE VISITOR ]

**WHO IS:**
[ GIVE SOME CONTEXT (E.G. WHO IS USING MY MOBILE) ],

**I want to:**
[ WHAT DO YOU WANT TO DO? ],

**so that:**
[ WHY? ].


#### HELP
User stories help define what is expected from a new feature and help project managers consider it's priority with regard to planning and, developers to fulfil expectations.

Keep user stories shaort and sweet. Example:

As a [USER] Who is [USING THE IPHONE APP], I want to [SEE THE MENU IN THE TOP LEFT CORNER OF THE APP], so that [I DONT HAVE TO LEARN SOME NEW FORM OF NAVIGATION WHICH IS INCONSISTENT WITH OTHER APPS]