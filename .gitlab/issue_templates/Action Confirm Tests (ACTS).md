### ACTION CONFIRMATION TEST (ACT)
**What is being tested?**
[ DESCRIBE THE TEST ]

[START]
[A]
[C]
[END]


#### HELP
Action/Confirmation tests help other understand what steps they are supposed to take to perform a certain task. You can reference certain items using the [#KEY] syntax.

All tests start with an action.
All Tests end with a confirmation.

Here's an example:

[START]
[A] Open path/to/page in Browser [#0]
[C] Page loads with no errors
[A] Click on the menu in the top left corner of the page [#1]
[C] [#1] opens
[C] [#1] has item "Open"
[A] Click "Open" in [#1]
[C] New page [#2] opens in [#0]
[C] Page title in [#2] is "The title"
[END]

###### ACTS INDEX
[START] = Begining of test sequence
[A] = Action
[C] = Confirmation
[#...] = References
[END] = End of test sequence